/*
 * COPYRIGHT (C) 2020   CHIPS&MEDIA INC. ALL RIGHTS RESERVED
 *
 * This file is distributed under BSD 3 clause and LGPL2.1 (dual license)
 * SPDX License Identifier: BSD-3-Clause
 * SPDX License Identifier: LGPL-2.1-only
 *
 * The entire notice above must be reproduced on all authorized copies.
 *
 * File         : $Id: vpu_dec.h 208253 2020-09-01 09:11:42Z nas.chung $
 * Description  :
 */
#ifndef __VPU_DEC_DRV_H__
#define __VPU_DEC_DRV_H__

#include "vpu.h"

#define VPU_DEC_DEV_NAME "C&M VPU Decoder"
#define VPU_DEC_DRV_NAME "vpu-dec"

#define V4L2_CID_VPU_THUMBNAIL_MODE (V4L2_CID_USER_BASE + 0x1001)

int  vpu_dec_register_device(struct vpu_device *dev);
void vpu_dec_unregister_device(struct vpu_device *dev);
#endif

