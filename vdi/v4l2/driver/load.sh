#!/bin/sh
#-----------------------------------------------------------------------------
# COPYRIGHT (C) 2020   CHIPS&MEDIA INC. ALL RIGHTS RESERVED
# 
# This file is distributed under BSD 3 clause and LGPL2.1 (dual license)
# SPDX License Identifier: BSD-3-Clause
# SPDX License Identifier: LGPL-2.1-only
# 
# The entire notice above must be reproduced on all authorized copies.
# 
# Description  : 
#-----------------------------------------------------------------------------


module="vpu-v4l2"
device="vpu"
mode="664"

# Group: since distributions do	it differently,	look for wheel or use staff
if grep	'^staff:' /etc/group > /dev/null; then
    group="staff"
else
    group="wheel"
fi

modprobe v4l2-common
modprobe v4l2-mem2mem
modprobe videobuf2-v4l2
modprobe videobuf2-vmalloc
modprobe videobuf2-common
modprobe videobuf2-dma-contig
modprobe videobuf2-memops

# invoke insmod	with all arguments we got
# and use a pathname, as newer modutils	do not look in .	by default
/sbin/insmod -f	./$module.ko $*	|| exit	-1
 
