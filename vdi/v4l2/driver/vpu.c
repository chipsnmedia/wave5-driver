/*
 * COPYRIGHT (C) 2020   CHIPS&MEDIA INC. ALL RIGHTS RESERVED
 *
 * This file is distributed under BSD 3 clause and LGPL2.1 (dual license)
 * SPDX License Identifier: BSD-3-Clause
 * SPDX License Identifier: LGPL-2.1-only
 *
 * The entire notice above must be reproduced on all authorized copies.
 *
 * File         : $Id: vpu.c 208253 2020-09-01 09:11:42Z nas.chung $
 * Description  :
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/firmware.h>
#include <linux/interrupt.h>
#include "vpu.h"
#include "vpu_dec.h"
#include "vpu_enc.h"
#include "coda9/coda9_regdefine.h"
#include "wave/wave5_regdefine.h"

#define VPU_PLATFORM_DEVICE_NAME    "vdec"
#define VPU_CLK_NAME                "vcodec"
#define VPU_FIRMWARE_NAME           "chagall.bin"

/* if the platform driver knows this driver */
/* the definition of VPU_REG_BASE_ADDR and VPU_REG_SIZE are not meaningful */
#define VPU_REG_BASE_ADDR    (0x75000000)
#define VPU_REG_SIZE         (0x4000*MAX_NUM_VPU_CORE)


#ifdef VPU_SUPPORT_RESERVED_VIDEO_MEMORY
#	define VPU_INIT_VIDEO_MEMORY_SIZE_IN_BYTE (62*1024*1024)
#	define VPU_DRAM_PHYSICAL_BASE 0x86C00000
#include "vmm.h"
struct vpudrv_buffer_t s_video_memory = {0};
#endif /*VPU_SUPPORT_RESERVED_VIDEO_MEMORY*/

static void vpu_clk_disable(struct clk *clk);
static int vpu_clk_enable(struct clk *clk);
static struct clk *vpu_clk_get(struct device *dev);
static void vpu_clk_put(struct clk *clk);

static struct clk *s_vpu_clk;

struct vpudrv_buffer_t s_vpu_register = {0};

unsigned int vpu_debug = 1;
module_param(vpu_debug, uint, 0644);

//static int vpu_resume(struct platform_device *pdev);
//static int vpu_suspend(struct platform_device *pdev, pm_message_t state);

int vpu_wait_interrupt(struct vpu_instance *inst, unsigned int timeout)
{
	int irq_status;

	spin_lock(&inst->dev->lock);

	wait_for_completion_timeout(&inst->dev->irq_done,
				    msecs_to_jiffies(timeout));
	if (inst->dev->irq_status > 0) {
		VPU_ClearInterruptEx(inst->handle, inst->dev->irq_status);
		irq_status = inst->dev->irq_status;
		inst->dev->irq_status = 0;
	} else {
		irq_status = -1;
	}

	DPRINTK(inst->dev, 1, "irq_status : %d\n", irq_status);

	reinit_completion(&inst->dev->irq_done);

	spin_unlock(&inst->dev->lock);

	return irq_status;
}

static irqreturn_t vpu_irq(int irq, void *dev_id)
{
	struct vpu_device   *dev = (struct vpu_device *)dev_id;
	struct vpu_instance *inst;

	if (vdi_read_register(0, W5_VPU_VPU_INT_STS)) {
		dev->irq_status = vdi_read_register(0, W5_VPU_VINT_REASON);
		vdi_write_register(0, W5_VPU_VINT_REASON_CLR, dev->irq_status);
		vdi_write_register(0, W5_VPU_VINT_CLEAR, 0x1);
	}

	if (dev->irq_status > 0) {
		inst = v4l2_m2m_get_curr_priv(dev->v4l2_m2m_dev);

		if (inst == NULL) {
			VLOG(ERR, "VPU instance not ready\n");
			complete(&dev->irq_done);
		} else {
			inst->ops->finish_process(inst);
		}
	} else {
		VLOG(ERR, "Interrupt not ready\n");
		return IRQ_HANDLED;
	}

	return IRQ_HANDLED;
}


static void vpu_device_run(void *priv)
{
	struct vpu_instance *inst = priv;

	DPRINTK(inst->dev, 1, "inst type=%d state=%d\n",
			       inst->type, inst->state);

	inst->ops->start_process(inst);
}

static int vpu_job_ready(void *priv)
{
	struct vpu_instance *inst = priv;

	DPRINTK(inst->dev, 1, "inst type=%d state=%d\n",
			       inst->type, inst->state);

	if (inst->state == VPU_INST_STATE_STOP)
		return 0;

	return 1;
}

static void vpu_job_abort(void *priv)
{
	struct vpu_instance *inst = priv;

	DPRINTK(inst->dev, 1, "inst type=%d state=%d\n",
			       inst->type, inst->state);

	inst->ops->stop_process(inst);
}

static const struct v4l2_m2m_ops vpu_m2m_ops = {
	.device_run = vpu_device_run,
	.job_ready  = vpu_job_ready,
	.job_abort  = vpu_job_abort,
};

static int vpu_load_firmware(struct device *dev)
{
	const struct firmware *fw;
	RetCode ret = RETCODE_SUCCESS;
	Uint32 version;
	Uint32 revision;
	Uint32 productId;

	if (request_firmware(&fw, VPU_FIRMWARE_NAME, dev)) {
		VLOG(ERR, "request_firmware fail\n");
		return -1;
	}

	ret = VPU_InitWithBitcode(0, (unsigned short *)fw->data, fw->size/2);
	if (ret != RETCODE_SUCCESS) {
		VLOG(ERR, "VPU_InitWithBitcode fail\n");
		return -1;
	}
	release_firmware(fw);

	ret = VPU_GetVersionInfo(0, &version, &revision, &productId);
	if (ret != RETCODE_SUCCESS) {
		VLOG(ERR, "VPU_GetVersionInfo fail\n");
		return -1;
	}

	VLOG(ERR, "ProductId : %08x\n", productId);
	VLOG(ERR, "FwVersion : %08x(r%d)\n", version, revision);

	return 0;
}

static int vpu_probe(struct platform_device *pdev)
{
	int err = 0;
	struct vpu_device *dev;
#ifdef VPU_SUPPORT_RESERVED_VIDEO_MEMORY
	unsigned int mem_size;
	unsigned long mem_virt;
	unsigned long mem_base;
#endif
	struct resource *res = NULL;
	unsigned int reg_size;
	unsigned long reg_virt;
	unsigned long reg_base;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);

	if (res) {
		reg_base = res->start;
		reg_size = res->end - res->start;
		reg_virt = (unsigned long)ioremap_nocache(reg_base, reg_size);
	} else {
		reg_base = VPU_REG_BASE_ADDR;
		reg_size = VPU_REG_SIZE;
		reg_virt = (unsigned long)ioremap_nocache(reg_base, reg_size);
	}

	s_vpu_register.phys_addr = reg_base;
	s_vpu_register.virt_addr = reg_virt;
	s_vpu_register.size      = reg_size;

	VLOG(ERR, "Platform driver phys_addr=0x%lx virt_addr=0x%lx\n",
		  s_vpu_register.phys_addr, s_vpu_register.virt_addr);

	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	if (!dev)
		goto ERROR_PROVE_DEVICE;

	dev_set_drvdata(&pdev->dev, dev);

	err = v4l2_device_register(&pdev->dev, &dev->v4l2_dev);
	if (err) {
		VLOG(ERR, "v4l2_device_register fail\n");
		goto ERROR_PROVE_DEVICE;
	}

	mutex_init(&dev->mutex);
	spin_lock_init(&dev->lock);
	init_completion(&dev->irq_done);

	dev->v4l2_m2m_dev = v4l2_m2m_init(&vpu_m2m_ops);
	if (IS_ERR(dev->v4l2_m2m_dev)) {
		VLOG(ERR, "v4l2_m2m_init fail\n");
		goto ERROR_PROVE_DEVICE;
	}

	err = vpu_dec_register_device(dev);
	if (err) {
		VLOG(ERR, "vpu_dec_register_device fail\n");
		goto ERROR_PROVE_DEVICE;
	}
	err = vpu_enc_register_device(dev);
	if (err) {
		VLOG(ERR, "vpu_enc_register_device fail\n");
		goto ERROR_PROVE_DEVICE;
	}

	if (pdev)
		s_vpu_clk = vpu_clk_get(&pdev->dev);
	else
		s_vpu_clk = vpu_clk_get(NULL);

	if (!s_vpu_clk)
		VLOG(ERR, "Not support clock controller.\n");
	else
		VLOG(ERR, "Get clock controller s_vpu_clk=%p\n", s_vpu_clk);

	vpu_clk_enable(s_vpu_clk);

	if (pdev)
		res = platform_get_resource(pdev, IORESOURCE_IRQ, 0);

	if (res) {
		dev->irq = res->start;
		VLOG(ERR, "platform driver irq number=0x%x\n", dev->irq);
	}

	err = request_threaded_irq(dev->irq, vpu_irq, NULL, 0, "vpu_irq", dev);
	if (err) {
		VLOG(ERR, "fail to register interrupt handler\n");
		goto ERROR_PROVE_DEVICE;
	}

#ifdef VPU_SUPPORT_RESERVED_VIDEO_MEMORY
	mem_size = VPU_INIT_VIDEO_MEMORY_SIZE_IN_BYTE;
	mem_base = VPU_DRAM_PHYSICAL_BASE;
	mem_virt = (unsigned long)ioremap_nocache(mem_base,
						  PAGE_ALIGN(mem_size));

	s_video_memory.size = mem_size;
	s_video_memory.phys_addr = mem_base;
	s_video_memory.base = mem_virt;
	if (!s_video_memory.base) {
		VLOG(ERR, "Fail to remap video memory\n");
		goto ERROR_PROVE_DEVICE;
	}

	VLOG(ERR, "Reserved video memory phys_addr=0x%lx base=0x%lx\n",
			s_video_memory.phys_addr, s_video_memory.base);
#else
	VLOG(ERR, "Need to implement customers memory allocator\n");
#endif


	if (vpu_load_firmware(&pdev->dev)) {
		VLOG(ERR, "failed to vpu_load_firmware\n");
		goto ERROR_PROVE_DEVICE;
	}

	return 0;

ERROR_PROVE_DEVICE:
	if (!dev) {
		vpu_dec_unregister_device(dev);
		vpu_enc_unregister_device(dev);
		v4l2_m2m_release(dev->v4l2_m2m_dev);
		v4l2_device_unregister(&dev->v4l2_dev);
		if (dev->irq)
			free_irq(dev->irq, dev);
		kfree(dev);
	}
#ifdef VPU_SUPPORT_RESERVED_VIDEO_MEMORY
	if (s_video_memory.base) {
		iounmap((void *)s_video_memory.base);
		s_video_memory.base = 0;
	}
#endif
	if (s_vpu_register.virt_addr)
		iounmap((void *)s_vpu_register.virt_addr);

	vpu_clk_disable(s_vpu_clk);
	vpu_clk_put(s_vpu_clk);

	return err;
}

static int vpu_remove(struct platform_device *pdev)
{
	struct vpu_device *dev = dev_get_drvdata(&pdev->dev);

#ifdef VPU_SUPPORT_RESERVED_VIDEO_MEMORY
	if (s_video_memory.base) {
		iounmap((void *)s_video_memory.base);
		s_video_memory.base = 0;
	}
#endif
	if (s_vpu_register.virt_addr)
		iounmap((void *)s_vpu_register.virt_addr);

	vpu_dec_unregister_device(dev);
	vpu_enc_unregister_device(dev);
	v4l2_m2m_release(dev->v4l2_m2m_dev);
	v4l2_device_unregister(&dev->v4l2_dev);
	vpu_clk_disable(s_vpu_clk);
	vpu_clk_put(s_vpu_clk);

	if (dev->irq)
		free_irq(dev->irq, dev);

	kfree(dev);
	return 0;
}

static struct platform_driver vpu_driver = {
	.driver = {
		.name = VPU_PLATFORM_DEVICE_NAME,
		},
	.probe = vpu_probe,
	.remove = vpu_remove,
	//.suspend = vpu_suspend,
	//.resume = vpu_resume,
};

static int __init vpu_init(void)
{
	int ret;

	ret = platform_driver_register(&vpu_driver);

	return ret;
}

static void __exit vpu_exit(void)
{
	platform_driver_unregister(&vpu_driver);
}

struct clk *vpu_clk_get(struct device *dev)
{
	return clk_get(dev, VPU_CLK_NAME);
}

void vpu_clk_put(struct clk *clk)
{
	if (!(clk == NULL || IS_ERR(clk)))
		clk_put(clk);
}

int vpu_clk_enable(struct clk *clk)
{
	if (!(clk == NULL || IS_ERR(clk))) {
		clk_enable(clk);
		return 1;
	}

	return 0;
}

void vpu_clk_disable(struct clk *clk)
{
	if (!(clk == NULL || IS_ERR(clk)))
		clk_disable(clk);
}

MODULE_DESCRIPTION("Chips&Media VPU V4L2 driver");
MODULE_LICENSE("GPL");

module_init(vpu_init);
module_exit(vpu_exit);

