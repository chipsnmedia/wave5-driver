/*
 * COPYRIGHT (C) 2020   CHIPS&MEDIA INC. ALL RIGHTS RESERVED
 *
 * This file is distributed under BSD 3 clause and LGPL2.1 (dual license)
 * SPDX License Identifier: BSD-3-Clause
 * SPDX License Identifier: LGPL-2.1-only
 *
 * The entire notice above must be reproduced on all authorized copies.
 *
 * File         : $Id: vpu.h 208253 2020-09-01 09:11:42Z nas.chung $
 * Description  :
 */
#ifndef __VPU_DRV_H__
#define __VPU_DRV_H__

#include <media/v4l2-device.h>
#include <media/v4l2-mem2mem.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-ioctl.h>
#include <media/v4l2-event.h>
#include <media/v4l2-fh.h>
#include <media/videobuf2-v4l2.h>
#include <media/videobuf2-dma-contig.h>
#include <media/videobuf2-vmalloc.h>
#include "../../../vpuapi/vpuconfig.h"
#include "../../../vpuapi/vpuapi.h"

// if this driver knows the dedicated video memory address
#define VPU_SUPPORT_RESERVED_VIDEO_MEMORY

#define DPRINTK(dev, level, fmt, args...) \
	v4l2_dbg(level, vpu_debug, &dev->v4l2_dev, "[%s]" fmt, __func__, ##args)

struct vpudrv_buffer_t {
	unsigned int  size;
	unsigned long phys_addr;
	unsigned long base;         // kernel logical address in use kernel
	unsigned long virt_addr;    // virtual user space address
};

struct vpu_device {
	struct v4l2_device   v4l2_dev;
	struct v4l2_m2m_dev *v4l2_m2m_dev;
	struct video_device  video_dev_dec;
	struct video_device  video_dev_enc;
	struct mutex         mutex;
	spinlock_t           lock;
	unsigned int         irq_status;
	int                  irq;
	struct completion    irq_done;
};

struct vpu_instance;

enum vpu_instance_type {
	VPU_INST_TYPE_DEC = 0,
	VPU_INST_TYPE_ENC = 1
};

enum vpu_instance_state {
	VPU_INST_STATE_NONE     = 0,
	VPU_INST_STATE_OPEN     = 1,
	VPU_INST_STATE_INIT_SEQ = 2,
	VPU_INST_STATE_PIC_RUN  = 3,
	VPU_INST_STATE_STOP     = 4,
	VPU_INST_STATE_WAIT_BUF = 5
};

struct vpu_instance_ops {
	void (*start_process)(struct vpu_instance *inst);
	void (*stop_process)(struct vpu_instance *inst);
	void (*finish_process)(struct vpu_instance *inst);
};

struct vpu_instance {
	struct v4l2_fh                 v4l2_fh;
	struct v4l2_ctrl_handler       v4l2_ctrl_hdl;
	struct vpu_device             *dev;

	struct v4l2_pix_format_mplane  src_fmt;
	struct v4l2_pix_format_mplane  dst_fmt;
	enum v4l2_colorspace           colorspace;
	enum v4l2_xfer_func            xfer_func;
	enum v4l2_ycbcr_encoding       ycbcr_enc;
	enum v4l2_quantization         quantization;
	enum v4l2_hsv_encoding         hsv_enc;

	enum vpu_instance_state        state;
	enum vpu_instance_type         type;
	const struct vpu_instance_ops *ops;

	VpuHandle                      handle;
	FrameBuffer                    frame_buf[MAX_REG_FRAME];
	vpu_buffer_t                   frame_vbuf[MAX_REG_FRAME];
	u32                            min_dst_frame_buf_count;
	u32                            queued_src_buf_num;
	u32                            queued_dst_buf_num;
	u64                            timestamp;

	//Decoder param
	struct mutex                   bitstream_mutex;
	vpu_buffer_t                   bitstream_vbuf;
	bool                           thumbnail_mode;

	//Encoder param
	unsigned int                   min_src_frame_buf_count;
	unsigned int                   rot_angle;
	unsigned int                   mirror_direction;
	unsigned int                   profile;
	unsigned int                   level;
	unsigned int                   bit_depth;
	unsigned int                   frame_rate;
	unsigned int                   vbv_buf_size;
	unsigned int                   min_qp_i;
	unsigned int                   max_qp_i;
	unsigned int                   min_qp_p;
	unsigned int                   max_qp_p;
	unsigned int                   min_qp_b;
	unsigned int                   max_qp_b;
};

struct vpu_buffer {
	struct v4l2_m2m_buffer v4l2_m2m_buf;
	bool                   consumed;
};

enum vpu_format_type {
	VPU_FMT_TYPE_CODEC = 0,
	VPU_FMT_TYPE_RAW   = 1
};

struct vpu_format {
	unsigned int v4l2_pix_fmt;
	unsigned int num_planes;
	unsigned int max_width;
	unsigned int min_width;
	unsigned int max_height;
	unsigned int min_height;
};

#ifdef VPU_SUPPORT_RESERVED_VIDEO_MEMORY
extern struct vpudrv_buffer_t s_video_memory;
#endif
extern struct vpudrv_buffer_t s_vpu_register;
extern unsigned int vpu_debug;

static inline struct vpu_instance *to_vpu_inst(struct v4l2_fh *vfh)
{
	return container_of(vfh, struct vpu_instance, v4l2_fh);
}

static inline struct vpu_instance *ctrl_to_vpu_inst(struct v4l2_ctrl *vctrl)
{
	return container_of(vctrl->handler, struct vpu_instance, v4l2_ctrl_hdl);
}

static inline struct vpu_buffer *to_vpu_buf(struct vb2_v4l2_buffer *vbuf)
{
	return container_of(vbuf, struct vpu_buffer, v4l2_m2m_buf.vb);
}

int vpu_wait_interrupt(struct vpu_instance *inst, unsigned int timeout);

#endif

