/*
 * COPYRIGHT (C) 2020   CHIPS&MEDIA INC. ALL RIGHTS RESERVED
 *
 * This file is distributed under BSD 3 clause and LGPL2.1 (dual license)
 * SPDX License Identifier: BSD-3-Clause
 * SPDX License Identifier: LGPL-2.1-only
 *
 * The entire notice above must be reproduced on all authorized copies.
 *
 * File         : $Id: vpu_enc.h 208253 2020-09-01 09:11:42Z nas.chung $
 * Description  :
 */
#ifndef __VPU_ENC_DRV_H__
#define __VPU_ENC_DRV_H__

#include "vpu.h"

#define VPU_ENC_DEV_NAME "C&M VPU Encoder"
#define VPU_ENC_DRV_NAME "vpu-enc"

int  vpu_enc_register_device(struct vpu_device *dev);
void vpu_enc_unregister_device(struct vpu_device *dev);
#endif

