//-----------------------------------------------------------------------------
// COPYRIGHT (C) 2020   CHIPS&MEDIA INC. ALL RIGHTS RESERVED
// 
// This file is distributed under BSD 3 clause and LGPL2.1 (dual license)
// SPDX License Identifier: BSD-3-Clause
// SPDX License Identifier: LGPL-2.1-only
// 
// The entire notice above must be reproduced on all authorized copies.
// 
// Description  : 
//-----------------------------------------------------------------------------

#include <stdarg.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include "vpuconfig.h"
#include "vputypes.h"

#include "../vdi_osal.h"

#define FILE_BUFFER_BASE 0x5000000
#define FILE_BUFFER_SIZE 1024*1024*10

typedef struct fileio_buf_t {
    char *_ptr;
    int   _cnt;
    char *_base;
    int   _flag;
    int   _file;
    int   _charbuf;
    int   _bufsiz;
    char *_tmpfname;
} fileio_buf_t;

static fileio_buf_t s_file;

int InitLog()
{
    return 1;
}

void DeInitLog()
{

}

void SetMaxLogLevel(int level)
{
}

int GetMaxLogLevel(void)
{
    return -1;
}

void LogMsg(int level, const char *format, ...)
{
    va_list ptr;

    va_start(ptr, format);
    vprintk(format, ptr);
    va_end(ptr);
}

void osal_init_keyboard()
{

}

void osal_close_keyboard()
{

}

void* osal_memcpy(void * dst, const void * src, int count)
{
    return memcpy(dst, src, count);
}

int osal_memcmp(const void* src, const void* dst, int size)
{
    return memcmp(src, dst, size);
}

void* osal_memset(void *dst, int val, int count)
{
    return memset(dst, val, count);
}

void* osal_malloc(int size)
{
    return kmalloc(size, GFP_KERNEL);
}

void* osal_realloc(void* ptr, int size)
{
    return NULL;
}

void osal_free(void *p)
{
    return kfree(p);
}

int osal_fflush(osal_file_t fp)
{
    return 1;
}

int osal_feof(osal_file_t fp)
{
    return 0;
}

osal_file_t osal_fopen(const char * osal_file_tname, const char * mode)
{
    s_file._base = (char *)FILE_BUFFER_BASE;
    s_file._bufsiz = FILE_BUFFER_SIZE;
    s_file._ptr = (char *)0;
    return &s_file;
}
size_t osal_fwrite(const void * p, int size, int count, osal_file_t fp)
{
    return 0;
}
size_t osal_fread(void *p, int size, int count, osal_file_t fp)
{

    return 0;
}

long osal_ftell(osal_file_t fp)
{
    return s_file._bufsiz;
}

int osal_fseek(osal_file_t fp, long offset, int origin)
{

    return offset;
}
int osal_fclose(osal_file_t fp)
{
    s_file._base = (char *)FILE_BUFFER_BASE;
    s_file._bufsiz = FILE_BUFFER_SIZE;
    s_file._ptr = (char *)0;

    return 1;
}
int osal_fscanf(osal_file_t fp, const char * _Format, ...)
{
    return 1;
}

int osal_fprintf(osal_file_t fp, const char * _Format, ...)
{
    va_list ptr;

    va_start( ptr, _Format);

    va_end(ptr);

    return 1;
}

int osal_kbhit(void)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
    return 0;
}
int osal_getch(void)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
    return -1;
}

int osal_flush_ch(void)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
    return -1;
}

void osal_msleep(Uint32 millisecond)
{
    Uint32 countDown = millisecond;
    while (countDown > 0) countDown--;
    VLOG(WARN, "<%s:%d> Please implemenet osal_msleep()\n", __FUNCTION__, __LINE__);
}

osal_thread_t osal_thread_create(void(*start_routine)(void*), void*arg)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
    return NULL;
}

Int32 osal_thread_join(osal_thread_t thread, void** retval)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
    return -1;
}

Int32 osal_thread_timedjoin(osal_thread_t thread, void** retval, Uint32 millisecond)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
    return 2;   /* error */
}

osal_mutex_t osal_mutex_create(void)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
    return NULL;
}

void osal_mutex_destroy(osal_mutex_t mutex)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
}

BOOL osal_mutex_lock(osal_mutex_t mutex)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
    return FALSE;
}

BOOL osal_mutex_unlock(osal_mutex_t mutex)
{
    return FALSE;
}

osal_sem_t osal_sem_init(Uint32 count)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
    return NULL;
}

void osal_sem_destroy(osal_sem_t sem)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
}

void osal_sem_wait(osal_sem_t sem)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
}

void osal_sem_post(osal_sem_t sem)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
}

Uint64 osal_gettime(void)
{
    VLOG(WARN, "<%s:%d> NEED TO IMPLEMENT %s\n", __FUNCTION__, __LINE__, __FUNCTION__);
    return 0ULL;
}

